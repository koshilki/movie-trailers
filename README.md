# Movie Search Application

Search for movies on TMDB and watch trailers and other related videos

## Built with

- Express.js
- Typescript
- GraphQL
- React
- Bootstrap

## Configuration

You need to obtain API keys from TheMovieDB and Vimeo to enable all the functionality.

- https://developers.themoviedb.org/3/getting-started/authentication
- https://developer.vimeo.com/api/authentication#obtaining-an-access-token

Credentials should be added to `./api/.env` file, so API can use them for requests

## Running with Docker

Simply run `docker-compose up --build` from the root of the project. It'll start up API and Web application. App will be available on `http://localhost:8080`

## Running with Node

Both API and Webapp should be started separately. Run `npm start` in separate terminals in `./api` and `./webapp` folders.
