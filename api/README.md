# Movie Search API

The main purpose of this package is to provide a middleware for access to external services.

## Built with

- Express.js
- Typescript
- GraphQL

## Configuration

You need to obtain API keys from TheMovieDB and Vimeo to enable all the functionality.

- https://developers.themoviedb.org/3/getting-started/authentication
- https://developer.vimeo.com/api/authentication#obtaining-an-access-token

Credentials should be added to `.env` file, so API can use them for requests
