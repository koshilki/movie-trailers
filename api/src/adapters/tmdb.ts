import axios from 'axios';
import humps from 'humps';

export default ({ tmdb: tmdbConfig }: any): Object => {
  return {
    async getVideosByMovieId (movieId: String) {
      if (!tmdbConfig.apiKey) {
        console.log('No API key for TMDB found')
        return []
      }
      console.log(`https://api.themoviedb.org/3/movie/${movieId}/videos?api_key=<hidden>`)
      const { data: { results }} = await axios.get(`https://api.themoviedb.org/3/movie/${movieId}/videos?api_key=${tmdbConfig.apiKey}`)
      return humps.camelizeKeys(results)
    },

    async findMovies (query: String, queryPage: Number) {
      if (!tmdbConfig.apiKey) {
        console.log('No API key for TMDB found')
        return []
      }
      console.log(`https://api.themoviedb.org/3/search/movie?api_key=<hidden>&query=${query}`);
      const { data: { results }} = await axios.get(`https://api.themoviedb.org/3/search/movie?api_key=${tmdbConfig.apiKey}&query=${query}`)
      return humps.camelizeKeys(results)
    }
  }
}
