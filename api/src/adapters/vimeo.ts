import axios from 'axios';
import humps from 'humps';

const base64encode = (str: string): string => {
  const buff = Buffer.from(str);
  return buff.toString('base64')
}

export default ({vimeo: vimeoConfig}: any): Object => {
  const credentials = base64encode(`${vimeoConfig.clientId}:${vimeoConfig.clientSecret}`);
  return {
    async findVideos (query: String) {
      if (!vimeoConfig.clientId || !vimeoConfig.clientSecret) {
        console.log('No Vimeo clientId or secret found')
        return []
      }

      console.log(`https://api.vimeo.com/videos?query=${query}+movie+trailer&per_page=10&page=1`)
      const { data: { data: results } } = await axios.get(
        `https://api.vimeo.com/videos?query=${query}+movie+trailer&per_page=10&page=1`, { // Limit to first 10 results
          headers: {
            'Authorization': `basic ${credentials}`
          }
        }
      )
      return humps.camelizeKeys(results)
    }
  }
}
