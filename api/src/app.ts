import dotenv from 'dotenv';
import express from "express";
import bodyParser from "body-parser";
import graphqlHTTP from 'express-graphql';
import cors from 'cors';
import trailerSearchSchema from './schema';
import tmdbAdapter from './adapters/tmdb';
import vimeoAdapter from './adapters/vimeo';

dotenv.config();
import config from './config/index';

const app = express();

app.set("port", process.env.PORT || 3000);
app.use(cors(config.corsOptions))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api", (req, res) => {
    return graphqlHTTP({
        schema: trailerSearchSchema,
        graphiql: true,
        context: {
            tmdb: tmdbAdapter(config),
            vimeo: vimeoAdapter(config)
        }
    })(req, res)
});

export default app;
