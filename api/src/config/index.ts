import tmdb from './tmdb'
import vimeo from './vimeo'

const corsOptions = {
  origin: 'http://localhost:8080'
}


export default {
  tmdb,
  vimeo,
  corsOptions,
}
