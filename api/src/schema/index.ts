import {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql'

import TMDBMovie from './types/tmdb-movie';
import TMDBVideo from './types/tmdb-video';

const RootQueryType = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: () => {
    return {
      tmdbVideos: {
        type: new GraphQLList(TMDBVideo),
        description: 'Movie video from TheMovieDatabase',
        args: {
          movieId: { type: new GraphQLNonNull(GraphQLID) }
        },
        resolve: (obj, args, { tmdb }) => {
          return tmdb.getVideosByMovieId(args.movieId);
        }
      },
      movieSearch: {
        type: new GraphQLList(TMDBMovie),
        description: 'Movie search on TheMovieDatabase',
        args: {
          query: { type: new GraphQLNonNull(GraphQLString) }
        },
        resolve (obj, args, { tmdb }) {
          return tmdb.findMovies(args.query);
        }
      },
    }
  }
});

const ncSchema = new GraphQLSchema({
  query: RootQueryType,
})

export default ncSchema;
