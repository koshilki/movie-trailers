import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLList,
} from 'graphql'
import TMDBVideo from './tmdb-video'
import VimeoVideo from './vimeo-video'

export default new GraphQLObjectType({
  name: 'TMDBMovie',
  fields: () => {
    return {
      id: { type: GraphQLID },
      title: { type: new GraphQLNonNull(GraphQLString) },
      releaseDate: { type: GraphQLString },
      overview: { type: GraphQLString },
      posterPath: { type: GraphQLString },
      voteAverage: { type: GraphQLString },
      tmdbVideos: {
        type: new GraphQLList(TMDBVideo),
        resolve (obj, args, { tmdb }) {
          return tmdb.getVideosByMovieId(obj.id)
        }
      },
      vimeoVideos: {
        type: new GraphQLList(VimeoVideo),
        resolve (obj, args, { vimeo }) {
          return vimeo.findVideos(`${obj.title} Trailer`)
        }
      }
    }
  },
})
