import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLNonNull,
  GraphQLInt,
} from 'graphql'

import VideoType from './video-type';

export default new GraphQLObjectType({
  name: 'TMDBVideo',
  fields: () => {
    return {
      id: { type: GraphQLID },
      name: { type: GraphQLString },
      site: { type: GraphQLString },
      key: { type: GraphQLString },
      type: { type: VideoType },
    }
  },
})
