import {
  GraphQLEnumType
} from 'graphql'

export default new GraphQLEnumType({
  name: 'VideoType',
  values: {
    TRAILER: { value: 'Trailer' },
    TEASER: { value: 'Teaser' },
    CLIP: { value: 'Clip' },
    FEATURETTE: { value: 'Featurette' },
    BEHIND_THE_SCENES: { value: 'Behind the Scenes' },
    BLOOPERS: { value: 'Bloopers' },
  }
})
