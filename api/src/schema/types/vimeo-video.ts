import {
  GraphQLObjectType,
  GraphQLString,
} from 'graphql'

export default new GraphQLObjectType({
  name: 'VimeoVideo',
  fields: () => {
    return {
      link: { type: GraphQLString },
      name: { type: GraphQLString }
    }
  },
})
