import request from "supertest";
import app from "../src/app";

describe("GET /api", () => {
    it("should return 400 Bad Request", () => {
        return request(app).get("/api")
            .expect(400);
    });
});

describe('GET /api?query={movieSearch(query:"wag the dog"){title}}', () => {
    it("should return 200 OK", () => {
        return request(app).get('/api?query={movieSearch(query:"wag the dog"){title}}')
            .expect(200);
    });
});
