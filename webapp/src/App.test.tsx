import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders search box', () => {
  const { getByText } = render(<App />);
  const headerElement = getByText(/movie search/i);
  expect(headerElement).toBeInTheDocument();
})
