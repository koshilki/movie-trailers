import React, { useState } from 'react';
import './App.css';
import './components/Search'
import Search from './components/Search';
import { MovieProps, Movie } from './components/Movie';

export const App: React.FunctionComponent<{}> = () => {
  const [movies, setMovies] = useState([]);

  const handleSearch = (query: string) => {
    return fetch(`http://localhost:3000/api?query={movieSearch(query:"${query}"){title,releaseDate,voteAverage,posterPath,overview,tmdbVideos{name,site,key,type},vimeoVideos{name,link}}}`)
      .then(response => response.json())
      .then(json => {
        setMovies(json.data.movieSearch);
      });
  }
  return (
    <div className="App">
      <header className="App-header jumbotron">
        <h1 className="display-4">
          Movie search
        </h1>
        <p className="lead">Search for your favorite movies to get description and trailers</p>
        <hr/>
        <Search onSearch={handleSearch} />
      </header>
      <main className="App-main">
        {movies.map((movie, index) => {
          return (
            <Movie key={index} {...movie as MovieProps} />
          )
        })}
      </main>
    </div>
  );
};

export default App;
