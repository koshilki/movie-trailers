import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import Movie from './Movie';

test('renders movie entry', () => {
  const { getByText } = render(<Movie title="Test movie" tmdbVideos={[]} vimeoVideos={[]} />);
  expect(getByText('Test movie')).toBeInTheDocument();
});
