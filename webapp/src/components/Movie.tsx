import * as React from "react";
import "./Movie.css";
import { YoutubeVideoProps, YoutubeVideoCard } from './YoutubeVideo';
import { VimeoVideoProps, VimeoVideoCard } from './VimeoVideo';

export type MovieProps = {
  title: string;
  releaseDate?: string;
  overview?: string;
  posterPath?: string;
  voteAverage?: string;
  tmdbVideos: YoutubeVideoProps[];
  vimeoVideos: VimeoVideoProps[];
}

export const Movie: React.FunctionComponent<MovieProps> = ({ title, releaseDate, overview, tmdbVideos, vimeoVideos, posterPath }) => {
  const posterUrl = `http://image.tmdb.org/t/p/w342/${posterPath}`
  return (
    <div className="Movie card mb-3">
      <div className="row no-gutters">
        <div className="col-md-4">
          <img src={posterUrl} className="card-img" alt={`${title} poster`}/>
        </div>
        <div className="col-md-8">
          <div className="card-body">
          <h1>{title}</h1>
          {releaseDate ? <small className="text-muted">Released {releaseDate}</small> : ''}
          <p className="lead">{overview}</p>
          <hr />
          {tmdbVideos.length ? tmdbVideos.map((trailer, index) => {
            trailer.videoKey = trailer.key
            delete trailer.key
            return <YoutubeVideoCard key={index} {...trailer} />
          }) : <div className="alert alert-warning" role="alert">No YouTube videos available</div>}

          {vimeoVideos.length ? vimeoVideos.map((trailer, index) => {
            return <VimeoVideoCard key={index} {...trailer} />
          }) : <div className="alert alert-warning" role="alert">No Vimeo videos available</div>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Movie;
