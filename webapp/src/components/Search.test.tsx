import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import Search from './Search';

test('renders input with placeholder', () => {
  const { getByPlaceholderText } = render(<Search onSearch={() => {}}/>);
  const inputElement = getByPlaceholderText('Search query')
  expect(inputElement).toBeInTheDocument();
});
