import * as React from "react";
import "./Search.css";

const Search: React.FunctionComponent<{
  onSearch: Function;
}> = props => {
  function handleClick () {
    const inputBox = document.getElementById('search-box') as HTMLInputElement;
    const searchQuery = inputBox?.value;
    return props.onSearch(searchQuery)
  }
  return (
    <div className="input-group mb-3" style={{ width: 400 }}>
      <input
        type="text"
        className="form-control"
        placeholder="Search query"
        aria-label="Search query"
        aria-describedby="button-addon"
        id="search-box"
      />
      <div className="input-group-append">
        <button
          className="btn btn-primary"
          type="button"
          id="button-addon"
          onClick={handleClick}
        >
          Search
        </button>
      </div>
    </div>
  );
};

export default Search;
