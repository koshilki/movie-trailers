import React from 'react';
import { render } from '@testing-library/react';
import { VimeoVideoCard } from './VimeoVideo';

test('renders card with iframe', () => {
  const { getByTitle } = render(<VimeoVideoCard name="Test vimeo video" link="https://vimeo.com/testvideo"/>);
  const iframeElement = getByTitle('Test vimeo video')
  expect(iframeElement).toBeInTheDocument();
});
