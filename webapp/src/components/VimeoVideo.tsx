import * as React from 'react';

export type VimeoVideoProps = {
  name: string;
  link: string;
}

export const VimeoVideoCard: React.FunctionComponent<VimeoVideoProps> = ({ name, link }) => {
  const [, videoId] = link.split('vimeo.com/')
  return (
    <div className="card" style={{width: 320, display: "inline-block"}}>
      <iframe src={`https://player.vimeo.com/video/${videoId}?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;autopause=0&amp;player_id=0`} width="320" height="240" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen title={name}></iframe>
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
      </div>
    </div>
  )
}
