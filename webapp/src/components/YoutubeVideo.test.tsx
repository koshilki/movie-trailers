import React from 'react';
import { render } from '@testing-library/react';
import { YoutubeVideoCard } from './YoutubeVideo';

test('renders card with iframe', () => {
  const { getByTitle } = render(<YoutubeVideoCard name="Test youtube video" videoKey="testVideoKey" key="1"/>);
  const iframeElement = getByTitle('Test youtube video')
  expect(iframeElement).toBeInTheDocument();
});
