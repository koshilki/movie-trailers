import * as React from 'react';

export type YoutubeVideoProps = {
  id?: string;
  name: string;
  site?: string;
  key: string;
  videoKey: string;
  type?: string;
}

export const YoutubeVideoCard: React.FunctionComponent<YoutubeVideoProps> = ({ name, videoKey }) => {
  const url = `https://www.youtube.com/embed/${videoKey}`;
  return (
    <div className="card" style={{width: 320, display: "inline-block"}}>
      <iframe title={name} width="320" height="240" src={url} allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
      </div>
    </div>
  )
}
